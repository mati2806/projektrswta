from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import messages
from .forms import UserRegisterForm
from dysk.models import Disk, Directory
from django.conf import settings
from captcha.fields import ValidationError
import os

def register(request):
    
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')          
            messages.success(request, f'Account created for {username}!')
            return redirect('dysk-home')

        else:
            messages.warning(request, 'Check for errors and try again.')
            return render(request, 'users/register.html', {'form' : form})

    else:
        form = UserRegisterForm()

    return render(request, 'users/register.html', {'form' : form})

   
        

