from django import forms
from .models import Directory, Disk
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.core.validators import validate_image_file_extension


class UploadFileForm(forms.Form):
    newFile = forms.FileField()
    public = forms.BooleanField(required=False)

class CreateDirectoryForm(forms.Form):
    title = forms.CharField()

class EditFileForm(forms.Form):
    name = forms.CharField()
    directory = forms.ModelChoiceField(queryset=Directory.objects.all())
    public = forms.BooleanField(required=False)