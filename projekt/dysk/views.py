from django.shortcuts import render, redirect, get_object_or_404
from django.http import request, HttpResponseRedirect, HttpResponseForbidden, HttpResponse, Http404
from datetime import datetime
from .models import File, Directory
from .forms import UploadFileForm,  CreateDirectoryForm, EditFileForm
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.conf import settings
from django.views.generic import TemplateView
import os
import shutil


def download(request, pk):
    try:
        file = get_object_or_404(File, pk=pk)
        if not (request.user == file.owner or file.public):
            return HttpResponseForbidden('<h1>403 Forbidden</h1>', content_type='text/html')
        path = file.file_info.name
        file_path = os.path.join(settings.MEDIA_ROOT, path)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                    os.path.basename(file_path)
                return response
        raise Http404
    except Http404:
        messages.warning(request, f'File not found')
        return redirect('dysk-home')

def home(request):
    if request.user.is_authenticated:
        root = request.user.disk.mainDirectory
        files = request.user.files.filter(directory=root)
        dirs = request.user.dirs.filter(parent=root)
        request.user.disk.currentSize = getSize(request.user)
        request.user.disk.save()
        currentSizeP = round(request.user.disk.currentSize / request.user.disk.maxSize * 100 , 2)
        freeSpace = request.user.disk.maxSize - request.user.disk.currentSize
        freeSpaceP = 100 - currentSizeP
        return render(request, 'dysk/home.html', {
            'files': files,
            'dirs': dirs,
            'currentDir': root,
            'currentSizeP' : currentSizeP,
            'freeSpace' : freeSpace,
            'freeSpaceP' : freeSpaceP
            })
    else:
        return render(request, 'dysk/welcome.html')


def about(request):
    return render(request, 'dysk/about.html')


def fileDetailView(request, pk, dirId):
    try:
        file = get_object_or_404(File, pk=pk)
        currentDir = get_object_or_404(Directory, id=dirId)
        if request.user == file.owner:
            if request.method == 'POST':
                form = EditFileForm(request.POST)
                if form.is_valid():
                    file.directory = Directory.objects.filter(id=request.POST['directory']).first()
                    file.public = True if request.POST.get('public') == 'on' else False
                    file.file_info.name = request.POST['name']
                    file.save()
                    messages.success(request, f'File edited successfully!')
                    return redirect('dir-view', pk=currentDir.id)
            else:
                form = EditFileForm()
                dirs = request.user.dirs.all()
                form.fields['directory'].queryset = dirs
                form.fields['directory'].empty_label = None
                form.fields['directory'].initial = file.directory
                form.fields['name'].initial = file
                form.fields['public'].initial = file.public
                return render(request, 'dysk/file_edit.html', {'form': form})
        else:
            return HttpResponseForbidden('<h1>403 Forbidden</h1>', content_type='text/html')
    except Http404:
        messages.warning(request, f'File or directory not found')
        return redirect('dysk-home')
    except FileExistsError:
        messages.warning(request, f'This filename already exists!')
        return redirect('dir-view', pk=currentDir.id)
    except Exception as e:
        messages.warning(request, str(e))
        return redirect('dysk-home')




def getSize(user):
    files = File.objects.filter(owner=user)
    size = 0
    for file in files:
        size += file.file_info.size
    return size


class DirTemplateView(UserPassesTestMixin, TemplateView):
    template_name = 'dysk/home.html'
    context_object_name = 'context'

    def get_context_data(self, **kwargs):
        context = super(DirTemplateView, self).get_context_data(**kwargs)
        dirObject = get_object_or_404(Directory, id=self.kwargs.get('pk'))
        tempDir = dirObject.parent
        structure = []
        while tempDir != None:
            structure.append(tempDir)
            tempDir = tempDir.parent
        structure.reverse()
        currentSizeP = round(self.request.user.disk.currentSize / self.request.user.disk.maxSize * 100 , 2)
        freeSpace = self.request.user.disk.maxSize - self.request.user.disk.currentSize
        freeSpaceP = 100 - currentSizeP
        context.update({
            'files': File.objects.filter(directory = dirObject),
            'dirs': Directory.objects.filter(parent = dirObject),
            'currentDir' : dirObject,
            'structure' : structure,
            'currentSizeP' : currentSizeP,
            'freeSpace' : freeSpace,
            'freeSpaceP' : freeSpaceP
        })
        return context

    def test_func(self):
        dirObject = get_object_or_404(Directory, id=self.kwargs.get('pk'))
        if self.request.user == dirObject.owner:
            return True
        return False


def upload_file(request, pk):
    try:
        if request.user.is_authenticated:
            currentDir = get_object_or_404(Directory, id=pk)
            if currentDir.owner != request.user:
                return HttpResponseForbidden('<h1>403 Forbidden</h1>', content_type='text/html')

            if request.method == 'POST':
                form = UploadFileForm(request.POST, request.FILES)

                if form.is_valid():

                    public = True if request.POST.get('public') == 'on' else False
                    f = File(
                        file_info = request.FILES['newFile'],
                        directory = currentDir,
                        owner = request.user,
                        public = public
                        )

                    f.save()

                    messages.success(request, f'File uploaded!')
                    return redirect('dir-view', pk=pk)

                else:
                    messages.warning(request,f'File is invalid!')
                    return redirect('dir-view', pk=pk)

            else:
                form = UploadFileForm()
                form.fields['newFile'].label="Upload file:"
                return render(request, 'dysk/upload.html', {'form': form, 'currentDir': currentDir})
        else:
            return HttpResponseRedirect('/login/')

    except Exception as e:
        messages.warning(request, str(e))
        return redirect('dir-view', pk=pk)



def createDirectory(request, pk):
    try:
        if request.user.is_authenticated:
            currentDir = get_object_or_404(Directory, id=pk)
            if request.method == 'POST':
                form = CreateDirectoryForm(request.POST)
                if form.is_valid():
                        d = Directory(
                            directoryName = request.POST['title'],
                            parent = currentDir,
                            owner = request.user
                            )
                        d.save()
                        messages.success(request, f'Directory created!')
                        return redirect('dir-view', pk=pk)
            else:
                form = CreateDirectoryForm()
                return render(request, 'dysk/directoryCreate.html', {'form': form, 'currentDir': currentDir})
        else:
            return HttpResponseRedirect('/login/')
    except FileExistsError:
        messages.warning(request, f'Directory already exists, operation aborted')
        return redirect('dir-view', pk=pk)

def file_delete(request, pk):
    try:
        file = get_object_or_404(File, pk=pk)  # Get your current file
        returnDirId = file.directory.id
        if file.owner != request.user:
            return HttpResponseForbidden('<h1>403 Forbidden</h1>', content_type='text/html')
        else:
            file.delete()
            return redirect('dir-view', pk=returnDirId)

    except Http404:
        messages.warning(request, f'File not found')
        return redirect('dysk-home')



def file_copy(request, pk):
    try:
        file = get_object_or_404(File, pk=pk)
        returnDirId = file.directory.id
        if file.owner != request.user:
            return HttpResponseForbidden('<h1>403 Forbidden</h1>', content_type='text/html')
        else:
            try:
                file.duplicate()
                return redirect('dir-view', pk=returnDirId)
            except Exception as e:
                messages.warning(request, str(e))
                return redirect('dir-view', pk=returnDirId)



    except Http404:
        messages.warning(request, f'File not found')
        return redirect('dysk-home')

def dir_delete(request, pk):
    try:
        dir = get_object_or_404(Directory, pk=pk)  # Get your current file
        returnDirId = dir.parent.id

        if dir.owner != request.user:
            return HttpResponseForbidden('<h1>403 Forbidden</h1>', content_type='text/html')
        else:
            dir.delete()
            return redirect('dir-view', pk=returnDirId)

    except Http404:
        messages.warning(request, f'File not found')
        return redirect('dysk-home')
