from django.apps import AppConfig


class DyskConfig(AppConfig):
    name = 'dysk'
