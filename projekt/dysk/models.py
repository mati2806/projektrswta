from django.db import models
import datetime
from django.contrib.auth.models import User
from django.http import request
from django.views import generic
from django.core.files.base import ContentFile
from django.conf import settings
import shutil
from pathlib import Path
import os


class Directory(models.Model):
    directoryName = models.CharField(max_length=255)
    creationDate = models.DateTimeField(default=datetime.datetime.now, blank=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="dirs")

    def __str__(self):
        return self.getPath()

    def getPath(self):
        tmpDir = self
        path = self.directoryName
        while tmpDir.parent != None:
            tmpDir = tmpDir.parent
            path = tmpDir.directoryName + "/" + path
        return path

    class Meta:
        verbose_name = 'Directory'
        verbose_name_plural = 'Directiories'



class File(models.Model):
    file_info = models.FileField()
    directory = models.ForeignKey(Directory, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="files")
    public = models.BooleanField(default=False)
    creationDate = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
       return os.path.basename(self.file_info.name)

    def duplicate(self):
        newModel = File(directory=self.directory, owner=self.owner)
        new_file = ContentFile(self.file_info.read())
        filename, extension = os.path.splitext(str(self))
        new_file.name = filename + "_copy" + extension
        newModel.file_info = new_file
        newModel.save()


class Disk(models.Model):
    mainDirectory = models.OneToOneField(Directory, on_delete=models.CASCADE, null=True)
    owner = models.OneToOneField(User, on_delete=models.CASCADE, related_name="disk")
    maxSize = models.IntegerField(default=31457280)
    currentSize = models.IntegerField(default=0)

    def __str__(self):
        return self.owner.username + "_disk"







