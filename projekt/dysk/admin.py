from django.contrib import admin, messages
from .models import File, Directory, Disk
from django.contrib.auth.models import User
from .views import file_delete, dir_delete
from django.db.models.query import QuerySet
from django.db.models.signals import pre_delete, post_save, pre_save
from django.dispatch import receiver
from django.conf import settings
import shutil
import os


class FileAdmin(admin.ModelAdmin):

    def get_name(self, obj):
        return os.path.basename(obj.file_info.name)

    def get_size(self, obj):
        return round(obj.file_info.size / 1048576, 3)

    list_display = ('get_name', 'file_info', 'creationDate', 'get_size', 'owner', 'public')
    search_fields = ['owner__username', 'file_info', 'creationDate']

    get_name.short_description = 'Filename'
    get_size.short_description = 'Size (MB)'

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()


class DirectoryAdmin(admin.ModelAdmin):

    def get_location(self, obj):
        if obj.parent:
            return obj.parent.getPath()
        else:
            return "-"

    list_display = ['directoryName', 'get_location', 'owner', 'creationDate']
    get_location.short_description = 'Location'

    search_fields = ['directoryName', 'owner__username']

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()


class DiskAdmin(admin.ModelAdmin):

    def get_name(self, obj):
        return str(obj.owner) + '_disk'

    def get_load(self, obj):
        return round((obj.currentSize / obj.maxSize) * 100, 1)

    def get_currentSize(self, obj):
        return round(obj.currentSize / 1048576, 2)

    def get_maxSize(self, obj):
        return round(obj.maxSize / 1048576, 2)

    list_display = ['get_name', 'owner', 'get_load', 'get_currentSize', 'get_maxSize']

    get_name.short_description = 'Disk name'
    get_load.short_description = 'Load (%)'
    get_currentSize.short_description = 'Current Size (MB)'
    get_currentSize.admin_order_field = 'currentSize'

    get_maxSize.short_description = 'Max Size (MB)'
    get_maxSize.admin_order_field = 'maxSize'


admin.site.register(File, FileAdmin)
admin.site.register(Directory, DirectoryAdmin)
admin.site.register(Disk, DiskAdmin)


@receiver(pre_delete)
def adminDelete(sender, instance, using, **kwargs):
    if isinstance(instance, Directory):
        try:
            shutil.rmtree(settings.MEDIA_ROOT + "/" + str(instance) + "/")
        except FileNotFoundError:
            return
    if isinstance(instance, File):
        try:
            instance.owner.disk.currentSize -= instance.file_info.size
            instance.owner.disk.save()
            instance.file_info.delete(save=True)
        except Exception:
            return


@receiver(post_save)
def adminPostCreate(sender, instance, **kwargs):
    if kwargs['created']:
        if isinstance(instance, User):
            Disk.objects.create(
                mainDirectory=Directory.objects.create(
                    directoryName=instance.username + "_root",
                    owner=instance
                ),
                owner=instance
            )
        if isinstance(instance, Directory):
            os.mkdir(settings.MEDIA_ROOT + "/" + str(instance) + "/")


@receiver(pre_save)
def adminPreCreate(sender, instance, **kwargs):
    if isinstance(instance, Directory):
        directories = Directory.objects.filter(parent=instance.parent)
        for directory in directories:
            if str(instance) == str(directory) and directory.id != instance.id:
                raise FileExistsError
    if isinstance(instance, File):
        if instance._state.adding:
            instance.file_info.name = settings.MEDIA_ROOT + "/" + instance.directory.getPath() + "/" + str(instance)
            if instance.owner.disk.currentSize + instance.file_info.size > instance.owner.disk.maxSize:
                raise Exception('Not enough space!')
            else:
                instance.owner.disk.currentSize += instance.file_info.size
                instance.owner.disk.save()
        else:
            f = File.objects.filter(id=instance.id).first()

            files = File.objects.filter(directory=instance.directory)
            for file in files:
                if str(file) == str(instance) and file.id != instance.id:
                    raise FileExistsError

            srcPath = settings.MEDIA_ROOT + "/" + instance.directory.getPath() + "/" + str(instance)

            os.rename(f.file_info.name, srcPath)
            instance.file_info.name = srcPath
