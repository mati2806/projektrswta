
from django.urls import path
from . import views
from dysk.views import DirTemplateView
from django.conf.urls import url, include

urlpatterns = [
    path('', views.home, name='dysk-home'  ),
    path('about/', views.about, name='dysk-about'  ),
    path('upload/<int:pk>', views.upload_file, name='upload'),
    path('createDir/<int:pk>', views.createDirectory, name='createDir'),
    path('dir/<int:pk>/', DirTemplateView.as_view(), name='dir-view'),
    path('deleteFile/<int:pk>', views.file_delete, name='file_delete'),
    path('deleteDir/<int:pk>', views.dir_delete, name='dir_delete'),
    path('download/<int:pk>', views.download, name="file_download"),
    path('edit/<int:pk>/<int:dirId>', views.fileDetailView, name="file_edit"),
    path('copy/<int:pk>', views.file_copy, name="file_copy"),
    url(r'^captcha/', include('captcha.urls')),
]
